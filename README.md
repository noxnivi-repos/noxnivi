# NoxNivi



## Description
NoxNivi base Repository.
This repository holds the packages that shape the NoxNivi SET.

** this is still a Work In Progress** so be careful

** Warning ** : the packages are not signed yet. Eventually will in the near future. 
<!--
## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.
-->

<!--
## Screenshots
-->

## Installation
In order to use this repository you need to run Arch linux or an Arch derived distro. Then add the following lines to your `pacman.conf` file in order make pacman aware of this repo:

```
[noxnivi]
SigLevel = Optional TrustAll
Server = https://gitlab.com/noxnivi-repos/$repo/-/raw/main/$arch
```


## Usage
Once the repository has been added to `pacman` you can install the packages as with any other regular repository.

<!--
## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.
-->

## Roadmap
- Sign all packages

<!--
## Contributing

## Authors and acknowledgment


## License

## Project status
-->
