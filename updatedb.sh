#!/usr/bin/env bash
#
# script to update NoxNivi Repo database
# 2023.03 NoxNivi

# TODO: improve this script to cover more situations

cd x86_64

# remove old database files
rm noxnivi.db
rm noxnivi.files

# add all packages to the new database file
repo-add noxnivi.db.tar.gz *.pkg.tar.zst

# GitLab does not want symlinks to database so need to be removed
rm noxnivi.db
rm noxnivi.files

# now we have to rename de database files to the symlink names
# in order for the Repository to work properly
mv noxnivi.db.tar.gz noxnivi.db
mv noxnivi.files.tar.gz noxnivi.files

echo
echo "# # # >> Repository Database updated"

